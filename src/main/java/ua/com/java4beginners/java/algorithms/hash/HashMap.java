package ua.com.java4beginners.java.algorithms.hash;

public class HashMap {

    private int[] values;
    private int size = 0;
    private int capacity;

    public HashMap() {
        this.capacity = 10;
        this.values = new int[capacity];
    }

    public void put(int key, int val) {
        values[key % capacity] = val;
        size++;
    }

    public int get(int key) {
        return values[key % capacity];
    }

    public int size() {
        return size;
    }
}

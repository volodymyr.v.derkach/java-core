package ua.com.java4beginners.java.algorithms.hash;

import org.junit.Ignore;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class HashMapTest {

    @Test
    public void verifyMapCanStoreValue() {
        // here key works like 'hashCode' in java objects.
        HashMap map = new HashMap();
        map.put(1, 11);
        map.put(2, 50);

        assertThat(map.get(2), is(50));
    }

    @Ignore
    @Test
    public void verifyConflictResolving() {
        // when hashCode is the same (key % capacity), HashMap should resolve conflict.
        HashMap map = new HashMap();
        map.put(2, 10);
        map.put(22, 20);
        assertThat(map.get(2), is(10));
    }

    @Ignore
    @Test
    public void verifyResizing() {
        // the map have to grow twice in order to maintain constant performance O(1).
        HashMap map = new HashMap();
        int bigCapacity = 20;

        for (int i = 0; i < bigCapacity; i++) {
            map.put(i, i);
        }

        assertThat(map.size(), is(bigCapacity));
    }
}
